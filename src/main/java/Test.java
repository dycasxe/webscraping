import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVWriter;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.net.*;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) throws IOException, InterruptedException, URISyntaxException {

        ObjectMapper objectMapper = new ObjectMapper();
        String requestBody = "[\n" +
                "    {\n" +
                "        \"operationName\": \"DiscoveryComponentQuery\",\n" +
                "        \"variables\": {\n" +
                "            \"device\": \"desktop\",\n" +
                "            \"identifier\": \"clp_handphone-tablet_65\",\n" +
                "            \"componentId\": \"67\",\n" +
                "            \"filters\": \"{\\\"rpc_page_number\\\":\\\"1\\\",\\\"rpc_page_size\\\":\\\"100\\\",\\\"rpc_ProductId\\\":\\\"\\\",\\\"rpc_UserCityId\\\":\\\"176\\\",\\\"rpc_UserDistrictId\\\":\\\"2274\\\"}\"\n" +
                "        },\n" +
                "        \"query\": \"query DiscoveryComponentQuery($identifier: String!, $componentId: String!, $filters: String, $device: String = \\\"desktop\\\") {\\n  componentInfo(identifier: $identifier, component_id: $componentId, filters: $filters, device: $device) {\\n    data\\n    __typename\\n  }\\n}\\n\"\n" +
                "    }\n" +
                "]";

        CookieHandler.setDefault(new CookieManager());

        HttpCookie sessionCookie = new HttpCookie("Cookie", "_UUID_NONLOGIN_=3adc96c085e12a3003859e82b40bbc79; _UUID_NONLOGIN_.sig=JKwSfenpotFHYAOqjN5NNwKTDbo; _abck=FF6BE1E3E2418BE797C86157AB3267EF~-1~YAAQjgcFchuFBjqBAQAAChuPVwgJ6J6Cg1iNGR3VgdEaMJksobdEBooL3UPTr22b6Y6j/MssJ2ODJHOr3jKEuDuhGQDSMyLYVjoKkmf+U1COECkw4Y4vZA68cZrL8U+nPmiUBrb2BAbz/9XDU0wX7GpPyERZACtFFB9s5CoRvUUC85ai/eaX3ZT306f/dtkjYiwF1QwYDERZQdcQNwh031uGDeiu1Q8OlPs186RMiPi/jIHpG14AEA0L8DwGY0JzD+OFixbt1ymxk2tCkQb380VZuEWJJvvsHXGTyXCjK4G4OX6Z2eirFcpTdF4OwFflyS+zOQgQqkniK3HKmScbXWwW+lW22E+CJ9SOi8YtcLPfWF2HV2VX5QSFN9FOyW0gs4WvVmzrJjyO+Q==~-1~-1~-1; bm_sz=DAD4275D6D915EBB62D889DCA0D45FB1~YAAQjgcFchyFBjqBAQAAChuPVxDUwXFGfyA/6oCTCZe+aP8e1E0GzinXx2JpFmE3QqiZqGsyYUuth2h0in7AWNl6PCz02XF4C3C7DJKXSywTcw+wuCPsM1DSi2ekoTvKd6iUhYtuYmlYx+0UfzZ8TI/VRooiJC5XObCmPFZRwaNdvZvlAq5Hy8kw7qbzVQ21tputt/4Px9V0YzBJacPUX/YFgAIak68gCLN5Bfn7KYsQZFugq5aEbvN4Wjd8glgyuSZU1PtZdfBLcLoldTe8mfwD0UOqn6dpcR4NDg+kttJixs5PXlg=~4470578~4600134");
        ((CookieManager) CookieHandler.getDefault()).getCookieStore().add(new URI("https://gql.tokopedia.com/graphql/DiscoveryComponentQuery"),
                sessionCookie);

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://gql.tokopedia.com/graphql/DiscoveryComponentQuery"))
                .setHeader("User-Agent", "PostmanRuntime/7.28.4")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .header("Content-Type", "application/json")
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

        JSONArray root = new JSONArray(response.body());
        JSONObject index = root.getJSONObject(0);
        JSONObject data = index.getJSONObject("data").getJSONObject("componentInfo").getJSONObject("data").getJSONObject("component");
        JSONArray a = data.getJSONArray("data");

        int i;
        List<String[]> dataList = new ArrayList<>();
        String[] header = {"Nama", "Rating", "Store", "Harga", "Link Image", "Description"};
        dataList.add(header);
        for(i=0; i< a.length();i++){
            JSONObject aaa =a.getJSONObject(i);
            String[] aa = {aaa.getString("name"), aaa.getString("rating_average"),
                    aaa.getString("shop_url_mobile").split("/")[3],
                    aaa.getString("price"), aaa.getString("image_url_mobile"), null};
            dataList.add(aa);
        }

        try (CSVWriter writer = new CSVWriter(new FileWriter("/Users/dewy.yuliana/Documents/Workspace-Java/brick/webscraping/webscraping/test.csv"))) {
            writer.writeAll(dataList);
        }
    }
}
